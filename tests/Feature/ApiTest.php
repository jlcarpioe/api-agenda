<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * Test to check validation office works.
     *
     * @return void
     */
    public function test_office_validation_fail()
    {
        $response = $this->postJson('/api/offices', ['name' => '']);

        $response->assertStatus(400);
    }

    /**
     * Test to check free hours from a date.
     *
     * @return void
     */
    public function test_office_get_free_hours()
    {
        $response = $this->get('/api/offices/1');

        $response->assertStatus(200);
    }
}
