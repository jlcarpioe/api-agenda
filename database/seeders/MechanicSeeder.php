<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MechanicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // se puede usar factory pero solo necesito uno
        DB::table('mechanics')->insert([
            'name' => 'John Doe', 
            'email' => 'jdoe@example.com',
            'office_id' => 1 // error si no hay una oficina de ejempl
        ]);
    }
}
