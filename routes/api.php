<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\OfficeController;
use App\Http\Controllers\MechanicController;
use App\Http\Controllers\HoursTakenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('offices', OfficeController::class)->only(['index', 'store', 'show']);

Route::resource('mechanics', MechanicController::class)->only(['index', 'store']);

Route::resource('appointment', HoursTakenController::class)->only(['index', 'store']);
