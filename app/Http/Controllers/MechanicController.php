<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MechanicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mechanics = DB::table('mechanics')->select('id','name')->get();
        return response()->json(compact('mechanics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:mechanics',
            'office_id' => 'required|integer'
        ]);

        // TODO Falta verificar si la oficina existe

        if ($validator->fails()) {
            return response()->json(['success' => FALSE, 'errors' => $validator->errors()->all()], 400);
        }

        DB::table('mechanics')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'office_id' => $request->office_id
        ]);

        return response()->json(['success' => TRUE]);
    }
}
