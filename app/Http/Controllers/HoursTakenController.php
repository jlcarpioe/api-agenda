<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HoursTakenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('hours_taken')->get();
        return response()->json(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'date_hour' => 'required|date',
            'mechanic_id' => 'required|integer'
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => FALSE, 'errors' => $validator->errors()->all()], 400);
        }

        DB::table('hours_taken')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'date_taken' => $request->date_hour,
            'mechanic_id' => $request->mechanic_id
        ]);

        return response()->json(['success' => TRUE]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
