<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offices = DB::table('offices')->select('id','name')->get();
        return response()->json(compact('offices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:offices|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => FALSE, 'errors' => $validator->errors()->all()], 400);
        }

        DB::table('offices')->insert(['name' => $request->name]);

        return response()->json(['success' => TRUE]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // TODO Cambiar a modelo para facilitar la busqueda por las relaciones 

        // Solicitar fecha en la cual se desea agendar para ver devolver horas libres

        // Se puede solicitar tambien el empleado para mejorar el criterio de busqueda
        
        $office = DB::table('offices')->select('id','name')->where('id', $id)->get();

        return response()->json(['message' => 'Horarios disponibles de una fecha indicada', 'data' => $office]);
    }
}
